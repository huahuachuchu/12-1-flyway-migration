CREATE TABLE staff(
    id int auto_increment primary key,
    firstname varchar(128) not null ,
    lastname varchar(128) not null
);