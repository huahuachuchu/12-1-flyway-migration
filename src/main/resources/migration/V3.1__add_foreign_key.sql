ALTER TABLE contract ADD serviceID INT;
ALTER TABLE contract ADD FOREIGN KEY (serviceID) REFERENCES service (id);
ALTER TABLE contract ADD brandID INT;
ALTER TABLE contract ADD FOREIGN KEY (brandID) REFERENCES brand (id);
