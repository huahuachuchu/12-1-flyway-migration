CREATE TABLE contract_service_offices(
    officeID int auto_increment,
    contractID int,
    primary key (officeID,contractID),
    staffID int ,
    foreign key (officeID) references office(id),
    foreign key (contractID) references contract(id),
    foreign key (staffID) references staff(id)
);